.fl-node-<?php echo $id; ?> .bb-bridge-testimonial {
	color: #<?php echo $settings->color; ?>;
	border-style: <?php echo $settings->border_style; ?>;
    text-align: <?php echo $settings->alignment; ?>;

<?php if ($settings->font_family['family'] != 'Default'): ?>
	font-family: <?php echo $settings->font_family['family']; ?>;
	font-weight: <?php echo $settings->font_family['weight']; ?>;
<?php endif; ?>

<?php if ($settings->padding_top != ''): ?>
    padding-top: <?php echo $settings->padding_top . 'px'; ?>;
<?php endif; ?>

<?php if ($settings->padding_right != ''): ?>
    padding-right: <?php echo $settings->padding_right . 'px'; ?>;
<?php endif; ?>

<?php if ($settings->padding_bottom != ''): ?>
    padding-bottom: <?php echo $settings->padding_bottom . 'px'; ?>;
<?php endif; ?>

<?php if ($settings->padding_left != ''): ?>
    padding-left: <?php echo $settings->padding_left . 'px'; ?>;
<?php endif; ?>
	
<?php if ($settings->bg_color != '' AND $settings->background != 'none'): ?>
	background: <?php echo bb_bridge_color($settings->bg_color); ?>;
<?php endif; ?>

<?php if ($settings->border_color != ''): ?>
	border-color: <?php echo bb_bridge_color($settings->border_color); ?>;
<?php endif; ?>

<?php if ($settings->border_width_top != ''): ?>
    border-top-width: <?php echo $settings->border_width_top; ?>px;
<?php endif; ?>

<?php if ($settings->border_width_right != ''): ?>
    border-right-width: <?php echo $settings->border_width_right; ?>px;
<?php endif; ?>

<?php if ($settings->border_width_bottom != ''): ?>
    border-bottom-width: <?php echo $settings->border_width_bottom; ?>px;
<?php endif; ?>

<?php if ($settings->border_width_left != ''): ?>
    border-left-width: <?php echo $settings->border_width_left; ?>px;
<?php endif; ?>

<?php if ($settings->top_left != ''): ?>
    border-top-left-radius: <?php echo $settings->top_left; ?>px;
<?php endif; ?>

<?php if ($settings->top_right != ''): ?>
    border-top-right-radius: <?php echo $settings->top_right; ?>px;
<?php endif; ?>

<?php if ($settings->bottom_left != ''): ?>
    border-bottom-left-radius: <?php echo $settings->bottom_left; ?>px;
<?php endif; ?>

<?php if ($settings->bottom_right != ''): ?>
    border-bottom-right-radius: <?php echo $settings->bottom_right; ?>px;
<?php endif; ?>	
}

.fl-node-<?php echo $id; ?> .bb-bridge-testimonial a {
<?php if ($settings->link_color != ''): ?>
	color: #<?php echo $settings->link_color; ?>;
<?php endif; ?>
}

.fl-node-<?php echo $id; ?> .bb-bridge-testimonial a:hover {
<?php if ($settings->link_hover_color != ''): ?>
	color: #<?php echo $settings->link_hover_color; ?>;
<?php endif; ?>
}

<?php if ($settings->image == 'circle'): ?>
.fl-node-<?php echo $id; ?> .bb-bridge-testimonial-image img {
    border-radius: 50%;
}
<?php endif; ?>

<?php if ($settings->person_image_src != '' AND $settings->image_position == 'left'): ?>
.fl-node-<?php echo $id; ?> .bb-bridge-testimonial-wrap {
	margin-left: 100px;
}
.fl-node-<?php echo $id; ?> .bb-bridge-testimonial-image img {
    float: left;
	margin-right: 20px;
}
<?php endif; ?>

<?php if ($settings->person_image_src != '' AND $settings->image_position == 'right'): ?>
.fl-node-<?php echo $id; ?> .bb-bridge-testimonial-wrap {
	margin-right: 100px;
}
.fl-node-<?php echo $id; ?> .bb-bridge-testimonial-image img {
    float: right;
	margin-left: 20px;
}
<?php endif; ?>

<?php if ($settings->person_image_src != '' AND $settings->image_position == 'top'): ?>
.fl-node-<?php echo $id; ?> .bb-bridge-testimonial-image {
    text-align: center;
	margin-bottom: 20px;
}
<?php endif; ?>