.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider {
    border-style: <?php echo $settings->border_style; ?>;

<?php if ($settings->bg_color != '' AND $settings->background != 'none'): ?>
	background: <?php echo bb_bridge_color($settings->bg_color); ?>;
<?php endif; ?>

<?php if ($settings->border_color != ''): ?>
	border-color: <?php echo bb_bridge_color($settings->border_color); ?>;
<?php endif; ?>

<?php if ($settings->border_width_top != ''): ?>
    border-top-width: <?php echo $settings->border_width_top; ?>px;
<?php endif; ?>

<?php if ($settings->border_width_right != ''): ?>
    border-right-width: <?php echo $settings->border_width_right; ?>px;
<?php endif; ?>

<?php if ($settings->border_width_bottom != ''): ?>
    border-bottom-width: <?php echo $settings->border_width_bottom; ?>px;
<?php endif; ?>

<?php if ($settings->border_width_left != ''): ?>
    border-left-width: <?php echo $settings->border_width_left; ?>px;
<?php endif; ?>

<?php if ($settings->top_left != ''): ?>
    border-top-left-radius: <?php echo $settings->top_left; ?>px;
<?php endif; ?>

<?php if ($settings->top_right != ''): ?>
    border-top-right-radius: <?php echo $settings->top_right; ?>px;
<?php endif; ?>

<?php if ($settings->bottom_left != ''): ?>
    border-bottom-left-radius: <?php echo $settings->bottom_left; ?>px;
<?php endif; ?>

<?php if ($settings->bottom_right != ''): ?>
    border-bottom-right-radius: <?php echo $settings->bottom_right; ?>px;
<?php endif; ?>
}

<?php if ($settings->image == 'circle'): ?>
.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider img {
    border-radius: 50%;
}
<?php endif; ?>

.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider .bb-bridge-arrow {
    border-color: #<?php echo $settings->arrows_color; ?>;
    <?php if($settings->arrows == 'no'): ?>
    display: none;
    <?php endif; ?>
}

.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider .bb-bridge-arrow:hover {
    border-color: #<?php echo $settings->arrows_hover_color; ?>;
}

.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider h3 {
    color: #<?php echo $settings->color; ?>;
}

.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider span {
    color: #<?php echo $settings->position_color; ?>;
}

.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider p {
    color: #<?php echo $settings->text_color; ?>;
}

.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider .testim-dot {
    border-color: #<?php echo $settings->dots_color; ?>;
}

.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider .testim-dot.active,
.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider .testim-dot:hover {
    background: #<?php echo $settings->dots_color; ?>;
    border-color: #<?php echo $settings->dots_color; ?>;
}

.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider .testim-dot.active { 
    animation: testim-scale-<?php echo $id; ?> .5s ease-in-out forwards;   
}

@keyframes testim-scale-<?php echo $id; ?> {
    0% {
        box-shadow: 0px 0px 0px 0px #eee;
    }
    35% {
        box-shadow: 0px 0px 10px 5px #eee;        
    }
    70% {
        box-shadow: 0px 0px 10px 5px #<?php echo $settings->dots_color; ?>;
    }
    100% {
        box-shadow: 0px 0px 0px 0px #<?php echo $settings->dots_color; ?>;
    }
}

<?php if($settings->dots == 'no'): ?>
.fl-node-<?php echo $id; ?> .bb-bridge-tetimonial-slider .testim-dots {
    display: none;
}
<?php endif; ?>