<div class="bb-bridge-notification">
	<?php if ($settings->display_icon == 'yes'): ?>
		<i class="<?php echo $settings->icon; ?>"></i>
	<?php endif; ?>
	<div class="bb-bridge-notification-content">
		<span><?php echo $settings->title; ?></span>
		<p><?php echo $settings->description; ?></p>
	</div>
</div>