.fl-node-<?php echo $id; ?> .bb-bridge-notification {
	color: #<?php echo $settings->color; ?>;
	border-style: <?php echo $settings->border_style; ?>;
	
<?php if ($settings->bg_color != '' AND $settings->background != 'none'): ?>
	background: <?php echo bb_bridge_color($settings->bg_color); ?>;
<?php endif; ?>

<?php if ($settings->border_color != ''): ?>
	border-color: <?php echo bb_bridge_color($settings->border_color); ?>;
<?php endif; ?>

<?php if ($settings->border_width_top != ''): ?>
    border-top-width: <?php echo $settings->border_width_top; ?>px;
<?php endif; ?>

<?php if ($settings->border_width_right != ''): ?>
    border-right-width: <?php echo $settings->border_width_right; ?>px;
<?php endif; ?>

<?php if ($settings->border_width_bottom != ''): ?>
    border-bottom-width: <?php echo $settings->border_width_bottom; ?>px;
<?php endif; ?>

<?php if ($settings->border_width_left != ''): ?>
    border-left-width: <?php echo $settings->border_width_left; ?>px;
<?php endif; ?>

<?php if ($settings->top_left != ''): ?>
    border-top-left-radius: <?php echo $settings->top_left; ?>px;
<?php endif; ?>

<?php if ($settings->top_right != ''): ?>
    border-top-right-radius: <?php echo $settings->top_right; ?>px;
<?php endif; ?>

<?php if ($settings->bottom_left != ''): ?>
    border-bottom-left-radius: <?php echo $settings->bottom_left; ?>px;
<?php endif; ?>

<?php if ($settings->bottom_right != ''): ?>
    border-bottom-right-radius: <?php echo $settings->bottom_right; ?>px;
<?php endif; ?>	
}

.fl-node-<?php echo $id; ?> .bb-bridge-notification i {
	color: #<?php echo $settings->icon_color; ?>;
}

<?php if ($settings->display_icon == 'no'): ?>
.fl-node-<?php echo $id; ?> .bb-bridge-notification-content {
	margin-left: 0;
}
<?php endif; ?>	